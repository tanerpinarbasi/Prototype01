using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //Object wanted to spawn
    public GameObject enemyPrefab;
    //Spawn position of the object
    public GameObject spawnPoint;
    //Spawn the object after spawnTime
    public float spawnTime = 2.0f;

    private bool isEnemySpawned = false;
    void Start()
    {
        StartCoroutine(WaitAndSpawn(spawnTime));
    }

    void Update()
    {
        if(isEnemySpawned == true)
        {
            
        }

    }

    void SpawnEnemy()
    {
        Instantiate(enemyPrefab, spawnPoint.transform.position, Quaternion.identity);
        Debug.Log("Enemy is spawned");
        isEnemySpawned = true;
        
    }

    IEnumerator WaitAndSpawn(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            SpawnEnemy();
        }
    }
}
