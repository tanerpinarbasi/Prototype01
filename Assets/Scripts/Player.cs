using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 5.0f;
    private Rigidbody _playerRigidbody;
    [SerializeField] private int _health; 
    [SerializeField] private float _runSpeedMultiplier = 1.0f;

    private Vector3 _moveInput;
    private Vector3 _moveVelocity;
    private Camera mainCamera;




    // Start is called before the first frame update
    void Start()
    {
        _playerRigidbody = GetComponent<Rigidbody>();
        mainCamera = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        _moveInput = new Vector3(Input.GetAxisRaw("Horizontal"),0f,Input.GetAxisRaw("Vertical"));
        _moveVelocity = _moveInput * _moveSpeed;

        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLenghth;

        if(groundPlane.Raycast(cameraRay, out rayLenghth))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLenghth);
            Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

            transform.LookAt(new Vector3(pointToLook.x,transform.position.y,pointToLook.z));
        }
    }

    private void FixedUpdate()
    {
        _playerRigidbody.velocity = _moveVelocity;
    }

    void PlayerMovement()
    {

    }


}