using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float enemySpeed = 10.0f;
    private GameObject target;

    void Start()
    {
        
    }

    void Update()
    {
        ChaseTarget();
    }

    void ChaseTarget()
    {
        target = GameObject.Find("Player");
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.transform.position, enemySpeed * Time.deltaTime);
    }
}
